use Ferreteria;

/* Linux */
load data local infile '/home/rafael_zamora/Escritorio/BaseDatos.csv' into table Producto
fields terminated by ',' enclosed by '"'
lines terminated by '\n'; 
/* Mac os, pero tiene error la cosa :) usa mysql --local-infile=1 -u root -p */ 
load data local infile '/Users/RafaelZamora/Documents/Ferreteria/Base_De_Datos/BaseDatos.csv' into table Producto
fields terminated by ',' enclosed by '"'
lines terminated by '\n'; 

/* Nuevo empleado */
insert into Empleado (Nombre, Password, Usuario, Puesto) values ('Admin', sha1('Admin123'), 'Admin', 'Jefe');