-- MySQL Script generated by MySQL Workbench
-- mié 05 jun 2019 19:27:41 CDT
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Ferreteria
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Ferreteria` ;

-- -----------------------------------------------------
-- Schema Ferreteria
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Ferreteria` DEFAULT CHARACTER SET utf8 ;
USE `Ferreteria` ;

-- -----------------------------------------------------
-- Table `Ferreteria`.`Producto`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ferreteria`.`Producto` ;

CREATE TABLE IF NOT EXISTS `Ferreteria`.`Producto` (
  `Codigo_De_Barras` BIGINT(20) NULL,
  `Codigo` INT NOT NULL,
  `Clave` VARCHAR(45) NOT NULL,
  `Descripcion` LONGTEXT NOT NULL,
  `Unidad` VARCHAR(20) NOT NULL,
  `Precio_minimo` DOUBLE NOT NULL,
  `Precio_publico` DOUBLE NOT NULL,
  `Codigo_Sat` INT NULL,
  `Descripcion_Sat` LONGTEXT NULL,
  `Cantidad` INT NULL,
  UNIQUE INDEX `Codigo_De_Barras_UNIQUE` (`Codigo_De_Barras` ASC),
  PRIMARY KEY (`Codigo`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ferreteria`.`Vendedor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ferreteria`.`Vendedor` ;

CREATE TABLE IF NOT EXISTS `Ferreteria`.`Vendedor` (
  `idVendedor` INT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  `Password` LONGTEXT NOT NULL,
  `Rfc` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`idVendedor`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ferreteria`.`Venta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ferreteria`.`Venta` ;

CREATE TABLE IF NOT EXISTS `Ferreteria`.`Venta` (
  `idVenta` INT NOT NULL AUTO_INCREMENT,
  `Total` DOUBLE NOT NULL,
  `Fecha` DATETIME NOT NULL,
  `idVendedor` INT NOT NULL,
  PRIMARY KEY (`idVenta`),
  INDEX `idVendedor_idx` (`idVendedor` ASC),
  CONSTRAINT `idVendedor`
    FOREIGN KEY (`idVendedor`)
    REFERENCES `Ferreteria`.`Vendedor` (`idVendedor`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Ferreteria`.`Detalle_Venta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Ferreteria`.`Detalle_Venta` ;

CREATE TABLE IF NOT EXISTS `Ferreteria`.`Detalle_Venta` (
  `idVenta` INT NOT NULL,
  `idProducto` INT NOT NULL,
  INDEX `idVenta_idx` (`idVenta` ASC),
  INDEX `idProducto_idx` (`idProducto` ASC),
  CONSTRAINT `idVenta`
    FOREIGN KEY (`idVenta`)
    REFERENCES `Ferreteria`.`Venta` (`idVenta`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `idProducto`
    FOREIGN KEY (`idProducto`)
    REFERENCES `Ferreteria`.`Producto` (`Codigo`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
