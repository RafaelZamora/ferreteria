/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Objetos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author rafaelantonio
 */
public class Conexion {
    
    public static Connection getConnection(){
        Connection conexion = null;
        try {
            conexion = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/Ferreteria?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "root1234");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conexion;
    }
}
