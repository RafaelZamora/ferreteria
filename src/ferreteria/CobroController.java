/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class CobroController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private Label lbl1;

    @FXML
    private Label lbltotal;

    @FXML
    private JFXTextField TxtPago;

    @FXML
    private Label lbl2;

    @FXML
    private Label lblCambio;

    @FXML
    private JFXButton BtnFinalizar;

    @FXML
    private JFXTextField TxtDescuento;

    @FXML
    private TextArea TxtImprimir;

    @FXML
    private JFXTextArea TxtAgotados;

    private double Total = CajaRegistradoraController.Total;

    public static ArrayList<String> Agotados = new ArrayList<>();

    boolean flag = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        this.lbltotal.setText(String.valueOf(Total));

    }

    @FXML
    void Cambio(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtPago.getText())) {
            } else {
                this.lblCambio.setText(String.valueOf(Double.valueOf(this.TxtPago.getText()) - this.Total));

            }
        }
    }

    @FXML
    void Finalizar(MouseEvent event) {

    }

    @FXML
    void Descuento(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtDescuento.getText())) {

            } else {
                this.Total -= ((Total * Integer.parseInt(this.TxtDescuento.getText())) / 100);
                this.lbltotal.setText(String.valueOf(this.Total));
            }
        }
    }
}
