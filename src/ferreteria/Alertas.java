/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

/**
 *
 * @author RafaelZamora
 */
public class Alertas {
    @FXML
    public static void MostrarAlertas(int modo, StackPane StackPane){
        JFXDialogLayout Contenido = new JFXDialogLayout();
        Contenido.setHeading(new Text("Aviso"));
        switch (modo) {
            case 1:
                Contenido.setBody(new Text("Se ha actualizado el producto con éxito!"));
                break;
            case 2:
                Contenido.setBody(new Text("Se ha ingresado el producto con éxito!"));
                break;
            case 3:
                Contenido.setBody(new Text("Usuario o contraseña incorrectos!"));
                break;
            case 4:
                Contenido.setBody(new Text("Agregado con éxito!"));
                break;
            case 5:
                Contenido.setBody(new Text("Rellene todos los campos"));
                break;
            case 6:
                Contenido.setBody(new Text("No se ha encontrado el producto, registrelo antes de modificar"));
                break;
            case 7:
                Contenido.setBody(new Text("Venta registrada con éxito!"));
                break;
            case 8:
                Contenido.setBody(new Text("No puedes acceder a esta función porque no cumples los permisos necesarios"));
                break;
            case 9:
                Contenido.setBody(new Text("Empleado actualizado con éxito!"));
                break;
            case 10:
                Contenido.setBody(new Text("Productos modificados con éxito!"));
                break;
            case 11:
                Contenido.setBody(new Text("Empleado eliminado correctamente!"));
                break;
            case 12:
                Contenido.setBody(new Text("Se ha actualizado el margen correctamente!"));
                break;
            case 13:
                Contenido.setBody(new Text("Venta registrada con éxito!\nEstos productos están pronto a ser agotados\n" + 
                                            CobroController.Agotados.toString()));
                break;
        }
        JFXDialog Alerta = new JFXDialog(StackPane, Contenido, JFXDialog.DialogTransition.CENTER);
        JFXButton button = new JFXButton("Aceptar");
        button.setOnAction((ActionEvent event) -> {
            Alerta.close();
        });
        Contenido.setActions(button);
        Alerta.show();
    }
}
