/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;      
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.input.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author rafaelantonio
 */
public class AgregarProductoController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private JFXTabPane TabPane;

    @FXML
    private Tab TabActualizar;

    @FXML
    private AnchorPane RootActualizar;

    @FXML
    private JFXTextField TxtCodigoBarras;

    @FXML
    private JFXTextField TxtCodigo;

    @FXML
    private JFXTextField TxtPrecio;

    @FXML
    private JFXTextField TxtCantidad;

    @FXML
    private JFXComboBox<String> CbUnidad;

    @FXML
    private JFXTextArea TxtDesc;

    @FXML
    private JFXButton BtnActualizar;

    @FXML
    private JFXTextField TxtClave;

    @FXML
    private JFXTextField TxtBuscar;

    @FXML
    private Tab TabRegistro;

    @FXML
    private AnchorPane RootAgregar;

    @FXML
    private JFXTextField TxtCodigoBarrasA;

    @FXML
    private JFXTextField TxtCodigoA;

    @FXML
    private JFXTextField TxtPrecioA;

    @FXML
    private JFXTextField TxtCantidadA;

    @FXML
    private JFXComboBox<String> CbUnidadA;

    @FXML
    private JFXTextArea TxtDescA;

    @FXML
    private JFXButton BtnIngresar;

    @FXML
    private JFXTextField TXTMin;

    @FXML
    private JFXTextField TXTCodSat;

    @FXML
    private JFXComboBox<String> CBDescSat;

    @FXML
    private JFXTextField TxtClaveA;

    @FXML
    private JFXCheckBox CbxCodigo;
    
    private int Total;
    
    @FXML
    private Label a;

    @FXML
    private Label s;

    @FXML
    private Label d;

    @FXML
    private Label f;

    @FXML
    private Label g;

    @FXML
    private Label h;

    @FXML
    private Label j;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            CajaRegistradoraController.LProductos.clear();
            CargarUnidades();
            CargarDescripcionesSAT();
        } catch (SQLException ex) {
            Logger.getLogger(AgregarProductoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CargarUnidades() throws SQLException {
        Connection con = Conexion.getConnection();
        ResultSet rs = con.createStatement().executeQuery("select Distinct Unidad from Producto");
        while (rs.next()) {
            this.CbUnidadA.getItems().add(rs.getString("Unidad"));
            this.CbUnidad.getItems().add(rs.getString("Unidad"));
        }
    }

    public void CargarDescripcionesSAT() throws SQLException {
        Connection con = Conexion.getConnection();
        ResultSet rs = con.createStatement().executeQuery("select Distinct Descripcion_Sat from Producto");
        while (rs.next()) {
            this.CBDescSat.getItems().add(rs.getString("Descripcion_Sat"));
        }
    }

    @FXML
    void ActualizarProducto(MouseEvent event) {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            if (this.CbxCodigo.isSelected()) {
                //Buscar por código
                statement = con.prepareStatement("update Producto set Clave = ?, Precio_publico = ?, Cantidad = ?, Unidad = ?, Descripcion = ? where Codigo = ?");
                statement.setString(1, this.TxtClave.getText());
                statement.setDouble(2, Double.parseDouble(this.TxtPrecio.getText()));
                statement.setInt(3, Integer.parseInt(this.TxtCantidad.getText()));
                statement.setString(4, this.CbUnidad.getValue());
                statement.setString(5, this.TxtDesc.getText());
                statement.setString(6, this.TxtCodigo.getText());
            } else {
                //Buscar por código de barras
                statement = con.prepareStatement("update Producto set Clave = ?, Precio_publico = ?, Cantidad = ?, Unidad = ?, Descripcion = ? where Codigo_De_Barras = ?");
                statement.setString(1, this.TxtClave.getText());
                statement.setDouble(2, Double.parseDouble(this.TxtPrecio.getText()));
                statement.setInt(3, Integer.parseInt(this.TxtCantidad.getText()));
                statement.setString(4, this.CbUnidad.getValue());
                statement.setString(5, this.TxtDesc.getText());
                statement.setString(6, this.TxtCodigoBarras.getText());
            }
            statement.executeUpdate();
            statement = con.prepareStatement("update Restantes set Cantidad = ? where idProducto = ?");
            statement.setInt(1, Integer.parseInt(this.TxtCantidad.getText()));
            statement.setInt(2, Integer.parseInt(this.TxtCodigo.getText()));
            statement.executeUpdate();
            Alertas.MostrarAlertas(1, this.StackPane);
        } catch (SQLException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void IngresarProducto(MouseEvent event) {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("insert into Producto (Codigo_De_Barras, Codigo, Clave, Descripcion, Unidad, Precio_minimo, Precio_publico, Codigo_Sat, Descripcion_Sat, Cantidad) "
                    + "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            if ("".equals(this.TxtCodigoBarrasA.getText())) {
                statement.setNull(1, Types.NULL);
            } else {
                statement.setObject(1, new java.math.BigInteger(this.TxtCodigoBarrasA.getText()));
            }
            statement.setInt(2, Integer.parseInt(this.TxtCodigoA.getText()));
            statement.setString(3, this.TxtClaveA.getText());
            statement.setString(4, this.TxtDescA.getText());
            statement.setString(5, this.CbUnidadA.getValue());
            statement.setDouble(6, Double.parseDouble(this.TXTMin.getText()));
            statement.setDouble(7, Double.parseDouble(this.TxtPrecioA.getText()));
            if ("".equals(this.TXTCodSat.getText())) {
                statement.setNull(8, Types.NULL);
            } else {
                statement.setInt(8, Integer.parseInt(this.TXTCodSat.getText()));
            }
            statement.setString(9, this.TxtDescA.getText());
            if ("".equals(this.TxtCantidadA.getText())) {
                statement.setNull(10, Types.NULL);
            } else {
                statement.setInt(10, Integer.parseInt(this.TxtCantidadA.getText()));
            }
            statement.executeUpdate();
            Alertas.MostrarAlertas(2, this.StackPane);
        } catch (SQLException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void BuscarProducto(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            try {
                Connection con = Conexion.getConnection();
                PreparedStatement statement;
                if (this.CbxCodigo.isSelected()) {
                    //Buscar por código
                    statement = con.prepareStatement("select Codigo_De_Barras, Codigo, Clave, Descripcion, Unidad, Precio_publico, Cantidad from Producto where Codigo = ?");
                    statement.setString(1, this.TxtBuscar.getText());
                } else {
                    //Buscar por código de barras
                    statement = con.prepareStatement("select Codigo_De_Barras, Codigo, Clave, Descripcion, Unidad, Precio_publico, Cantidad from Producto where Codigo_De_Barras = ?");
                    statement.setString(1, this.TxtBuscar.getText());
                }
                ResultSet rs = statement.executeQuery();
                int total = 0;
                while (rs.next()) {
                    this.TxtCodigoBarras.setText(rs.getString("Codigo_De_Barras"));
                    this.TxtCodigo.setText(rs.getString("Codigo"));
                    this.TxtClave.setText(rs.getString("Clave"));
                    this.TxtDesc.setText(rs.getString("Descripcion"));
                    this.CbUnidad.setValue(rs.getString("Unidad"));
                    this.TxtPrecio.setText(rs.getString("Precio_publico"));
                    this.TxtCantidad.setText(rs.getString("Cantidad"));
                    total = 1;
                }
                if(total == 0){
                    Alertas.MostrarAlertas(6, this.StackPane);
                }
            } catch (SQLException ex) {
                Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
