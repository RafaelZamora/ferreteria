/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class ModificarUsuarioController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXTextField TxtBuscar;

    @FXML
    private JFXTextField TxtNombre;

    @FXML
    private JFXTextField TxtUsr;

    @FXML
    private JFXTextField TxtJerar;

    @FXML
    private JFXTextField TxtPass;
    
    @FXML
    private JFXButton BtnGuardar;

    @FXML
    private JFXButton BtnCerrar;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    void Buscar(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            try {
                Connection con = Conexion.getConnection();
                PreparedStatement statement;
                statement = con.prepareStatement("select Nombre, Usuario, NumJerarquia from Vendedor where idVendedor = ?;");
                statement.setInt(1, Integer.parseInt(this.TxtBuscar.getText()));
                ResultSet rs = statement.executeQuery();
                while(rs.next()){
                    this.TxtNombre.setText(rs.getString("Nombre"));
                    this.TxtUsr.setText(rs.getString("Usuario"));
                    this.TxtJerar.setText(String.valueOf(rs.getInt("NumJerarquia")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(ModificarUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @FXML
    void Cerrar(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    void Guardar(MouseEvent event) {
        try{
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("update Vendedor set Nombre = ?, Password = sha1(?), Usuario = ?, NumJerarquia = ? where idVendedor = ?;");
            statement.setString(1, this.TxtNombre.getText());
            statement.setString(2, this.TxtPass.getText());
            statement.setString(3, this.TxtUsr.getText());
            statement.setInt(4, Integer.parseInt(this.TxtJerar.getText()));
            statement.setInt(5, Integer.parseInt(this.TxtBuscar.getText()));
            statement.executeUpdate();
        } catch (Exception ex) {
            Alertas.MostrarAlertas(5, this.StackPane);
            Logger.getLogger(ModificarUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        Alertas.MostrarAlertas(9, this.StackPane);
        this.TxtBuscar.clear();
        this.TxtJerar.clear();
        this.TxtNombre.clear();
        this.TxtPass.clear();
    }
    
}
