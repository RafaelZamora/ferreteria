/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class LoginController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton BtnAgregar;

    @FXML
    private JFXButton BtnIngresar;

    @FXML
    private Label lbl;

    @FXML
    private JFXPasswordField TxtContra;

    @FXML
    private JFXTextField txtUsuario;

    public static boolean Login = false;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void Ingresar(MouseEvent event) throws IOException {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("select IdVendedor, Nombre, Usuario, Password, NumJerarquia from Vendedor where Usuario = ? and Password = sha1(?)");
            statement.setString(1, this.txtUsuario.getText());
            statement.setString(2, this.TxtContra.getText());
            ResultSet rs = statement.executeQuery();
            int resultado = 0;
            while (rs.next()) {
                MainController.Nombre = rs.getString("Nombre");
                MainController.IdVendedor = rs.getInt("IdVendedor");
                MainController.Jerarquia = rs.getInt("NumJerarquia");
                resultado = 1;
            }
            if (resultado == 1) {
                //Entrar al sistema 
                Stage Main = new Stage();
                Parent Mroot = FXMLLoader.load(getClass().getResource("FXMLMain.fxml"));
                Scene scene = new Scene(Mroot);
                Main.setScene(scene);
                Main.setResizable(false);
                Main.initStyle(StageStyle.UNDECORATED);
                Main.show();
                ((Node) event.getSource()).getScene().getWindow().hide();
            } else {
                //Negar acceso
                Alertas.MostrarAlertas(3, this.StackPane);
            }
        } catch (SQLException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void CrearUsuario(MouseEvent event) throws IOException {
        Stage Crear = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("Credenciales.fxml"));
        Scene scene = new Scene(root);
        Crear.setScene(scene);
        Crear.setResizable(false);
        Crear.initStyle(StageStyle.UNDECORATED);
        Crear.show();
    }

}
