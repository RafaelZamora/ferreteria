/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class CredencialesController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXTextField txtUsr;

    @FXML
    private JFXPasswordField txtPass;

    @FXML
    private JFXButton BtnAcceder;

    @FXML
    private JFXButton BtnCancelar;

    @FXML
    private Label lblText;

    public static int Resultado = 0;
    
    public static boolean Flag = false;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void Cancelar(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    void Ingresar(MouseEvent event) throws IOException {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("select Usuario, Password from Vendedor where Usuario = ? and Password = sha1(?) and NumJerarquia = 1");
            statement.setString(1, this.txtUsr.getText());
            statement.setString(2, this.txtPass.getText());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Resultado = 1;
            }
            if (Resultado == 0) {
                Alertas.MostrarAlertas(3, this.StackPane);
            } else {
                ((Node) event.getSource()).getScene().getWindow().hide();
                Stage Crear = new Stage();
                Parent root = FXMLLoader.load(getClass().getResource("CrearUsuario.fxml"));
                Scene scene = new Scene(root);
                Crear.setScene(scene);
                Crear.setResizable(false);
                Crear.initStyle(StageStyle.UNDECORATED);
                Crear.show();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CredencialesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
