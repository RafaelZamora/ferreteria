/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class EliminarEmpleadoController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXTextField TxtId;

    @FXML
    private JFXTextField TxtNom;

    @FXML
    private JFXButton BtnBorrar;

    @FXML
    private JFXButton BtnCerrar;

    @FXML
    private Label Label;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void Buscar(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtId.getText())) {

            } else {
                try {
                    Connection con = Conexion.getConnection();
                    PreparedStatement statement;
                    statement = con.prepareStatement("Select Nombre from Vendedor where idVendedor = ?");
                    statement.setInt(1, Integer.parseInt(this.TxtId.getText()));
                    ResultSet rs = statement.executeQuery();
                    while (rs.next()) {
                        this.TxtNom.setText(rs.getString("Nombre"));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EliminarEmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @FXML
    void Cerrar(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    void Eliminar(MouseEvent event) {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("delete from Vendedor where idVendedor = ?");
            statement.setInt(1, Integer.parseInt(this.TxtId.getText()));
            statement.executeUpdate();

            Alertas.MostrarAlertas(11, this.StackPane);
        } catch (SQLException ex) {
            Logger.getLogger(EliminarEmpleadoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
