/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class ModificarPrecioController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private Label Lbl;

    @FXML
    private JFXTextField TxtAu;

    @FXML
    private JFXTextField TxtDis;

    @FXML
    private JFXButton BtnCerrar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    void Aumentar(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtAu.getText())) {

            } else {
                try {
                    Connection con = Conexion.getConnection();
                    PreparedStatement statement;
                    statement = con.prepareStatement("update Producto set Precio_publico = Precio_publico + ((Precio_publico * ?) / 100)  where Cantidad is not null;");
                    statement.setInt(1, Integer.parseInt(this.TxtAu.getText()));
                    statement.executeUpdate();
                    Alertas.MostrarAlertas(10, this.StackPane);
                } catch (SQLException ex) {
                    Logger.getLogger(ModificarPrecioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @FXML
    void Cerrar(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    void Disminuir(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtDis.getText())) {

            } else {
                try {
                    Connection con = Conexion.getConnection();
                    PreparedStatement statement;
                    statement = con.prepareStatement("update Producto set Precio_publico = Precio_publico - ((Precio_publico * ?) / 100)  where Cantidad is not null;");
                    statement.setInt(1, Integer.parseInt(this.TxtDis.getText()));
                    statement.executeUpdate();
                    Alertas.MostrarAlertas(10, this.StackPane);
                } catch (SQLException ex) {
                    Logger.getLogger(ModificarPrecioController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
