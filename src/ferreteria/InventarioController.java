/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableColumn;
import com.jfoenix.controls.JFXTreeTableView;
import com.jfoenix.controls.RecursiveTreeItem;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeItem;

/**
 * FXML Controller class
 *
 * @author rafaelantonio
 */
public class InventarioController implements Initializable {

    @FXML
    private JFXTreeTableView<Producto> TablaProducto;
    
    @FXML
    private TreeTableColumn<Producto, String> CodProducto;

    @FXML
    private TreeTableColumn<Producto, String> Producto;

    @FXML
    private TreeTableColumn<Producto, String> Stock;

    @FXML
    private TreeTableColumn<Producto, String> PCompra;

    @FXML
    private TreeTableColumn<Producto, String> PVenta;

    @FXML
    private TreeTableColumn<Producto, String> Proveedor;

    @FXML
    private TreeTableColumn<Producto, String> Tel;

    @FXML
    private JFXTextField TxtFiltro;

    public static final ObservableList<Producto> LProductos = FXCollections.observableArrayList();
    Connection con = Conexion.getConnection();


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LProductos.clear();
        InicializarTabla();
        CargarLista();
    }
    
    private void InicializarTabla(){
        CodProducto.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetCodigo());
        Producto.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetNombre());
        Stock.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetStock());
        PCompra.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetPCompra());
        PVenta.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetPVenta());
        Proveedor.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetProveedor());
        Tel.setCellValueFactory((TreeTableColumn.CellDataFeatures<Producto, String> param) -> param.getValue().getValue().sGetTelefono());
        final TreeItem<Producto> root = new RecursiveTreeItem<>(LProductos, RecursiveTreeObject::getChildren);
        TablaProducto.setRoot(root);
        TablaProducto.setShowRoot(false);
        //Filtro
        this.TxtFiltro.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            TablaProducto.setPredicate((TreeItem<Producto> t) -> {
                Boolean flag = t.getValue().sGetNombre().getValue().contains(newValue);
                return flag;
            });
        });
    }

    private void CargarLista(){
        try{
            ResultSet rs = con.createStatement().executeQuery("select Clave, Descripcion, Cantidad, Precio_Compra, Precio_Venta, Proveedor.Nombre, Proveedor.Telefono" +
                    "from Producto inner join Proveedor where Producto.idProveedor = Proveedor.idProveedor");
            while (rs.next()){
                LProductos.add(new Producto(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7)));
            }
        } catch (SQLException e){
            e.printStackTrace();
        }
    }


    
    public class Producto extends RecursiveTreeObject<Producto>{
        StringProperty Codigo, Nombre, Stock, PCompra, PVenta, Proveedor, Telefono;
        public Producto(String Codigo, String Nombre, String Stock, String PCompra, String PVenta, String Proveedor, String Telefono){
            this.Codigo = new SimpleStringProperty(Codigo);
            this.Nombre = new SimpleStringProperty(Nombre);
            this.Stock = new SimpleStringProperty(Stock);
            this.PCompra = new SimpleStringProperty(PCompra);
            this.PVenta = new SimpleStringProperty(PVenta);
            this.Proveedor = new SimpleStringProperty(Proveedor);
            this.Telefono = new SimpleStringProperty(Telefono);
        }
        public StringProperty sGetCodigo() {return Codigo; }
        public StringProperty sGetNombre() {return Nombre; }
        public StringProperty sGetStock() {return Stock; }
        public StringProperty sGetPCompra() {return PCompra; }
        public StringProperty sGetPVenta() {return PVenta; }
        public StringProperty sGetProveedor() {return Proveedor; }
        public StringProperty sGetTelefono() {return Telefono; }
    }

    public class ProductoCompra extends RecursiveTreeObject<ProductoCompra>{
        StringProperty Num, Id, Producto, PCompra, Cantidad, Proveedor;
        public ProductoCompra(String Num, String Id, String Producto, String PCompra, String Cantidad, String Proveedor){
            this.Num = new SimpleStringProperty(Num);
            this.Id = new SimpleStringProperty(Id);
            this.Producto = new SimpleStringProperty(Producto);
            this.PCompra = new SimpleStringProperty(PCompra);
            this.Cantidad = new SimpleStringProperty(Cantidad);
            this.Proveedor = new SimpleStringProperty(Proveedor);
        }
    }
}
