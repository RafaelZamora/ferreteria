/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class ReporteController implements Initializable {

    @FXML
    private AnchorPane root;

    @FXML
    private JFXDatePicker FechaDe;

    @FXML
    private JFXDatePicker FechaHasta;

    @FXML
    private JFXButton BtnConsulta;

    @FXML
    private JFXTreeTableView<?> TreeView;
    
    @FXML
    private Label Lbl;

    @FXML
    private Label LblTotal;
    
    
    final ObservableList<?> LProductos = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
        CajaRegistradoraController.LProductos.clear();
        JFXTreeTableColumn<Reporte, String> idVenta = new JFXTreeTableColumn<>("Nº Venta");
        idVenta.setPrefWidth(83);
        idVenta.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SGetidVenta());
        JFXTreeTableColumn<Reporte, String> Codigo = new JFXTreeTableColumn<>("Código del producto");
        Codigo.setPrefWidth(200);
        Codigo.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetIdProducto());
        JFXTreeTableColumn<Reporte, String> Descripcion = new JFXTreeTableColumn<>("Descripción");
        Descripcion.setPrefWidth(430);
        Descripcion.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetDescripcion());
        JFXTreeTableColumn<Reporte, String> Precio = new JFXTreeTableColumn<>("Precio");
        Precio.setPrefWidth(150);
        Precio.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetPrecio());
        JFXTreeTableColumn<Reporte, String> Fecha = new JFXTreeTableColumn<>("Fecha");
        Fecha.setPrefWidth(150);
        Fecha.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetFecha());
        JFXTreeTableColumn<Reporte, String> Vendedor = new JFXTreeTableColumn<>("Vendedor"); 
        Vendedor.setPrefWidth(270);
        Vendedor.setCellValueFactory((TreeTableColumn.CellDataFeatures<Reporte, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetVendedor());
        final TreeItem<Reporte> root = new RecursiveTreeItem<>(LProductos, RecursiveTreeObject::getChildren);
        this.TreeView.getColumns().setAll(idVenta, Codigo, Descripcion, Fecha, Vendedor);
        this.TreeView.setRoot(root);
        this.TreeView.setShowRoot(false);*/
    }    
    
    @FXML
    void Cargar(MouseEvent event) {
        if(!"".equals(this.FechaDe.getValue().toString()) && !"".equals(this.FechaHasta.getValue().toString())){
            this.LProductos.clear();
            //Buscar
            try {
                Connection con = Conexion.getConnection();
                PreparedStatement statement;
                statement = con.prepareStatement("select dv.idVenta As 'idVenta', dv.idProducto As 'Codigo del producto', p.Descripcion As 'Descripcion' , concat('$ ', p.Precio_publico) As 'Precio', Fecha As 'Fecha', Nombre As 'Vendedor'\n" +
                                                "from (((Detalle_Venta dv inner join Producto p on dv.idProducto = p.Codigo) inner join Venta v on dv.idVenta = v.idVenta) \n" +
                                                "inner join Vendedor Ven on  v.idVendedor = Ven.idVendedor)\n" +
                                                "where v.Fecha between ? and ?;");
                statement.setString(1, this.FechaDe.getValue().toString());
                statement.setString(2, this.FechaHasta.getValue().toString());
                ResultSet rs = statement.executeQuery();

                statement = con.prepareStatement("select sum(Total) As Total from Venta where Fecha between ? and ? ;");
                statement.setString(1, this.FechaDe.getValue().toString());
                statement.setString(2, this.FechaHasta.getValue().toString());
                rs = statement.executeQuery();
                while(rs.next()){
                    this.LblTotal.setText(String.valueOf(rs.getInt("Total")));
                }
            } catch (SQLException ex) {
                Logger.getLogger(ReporteController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
