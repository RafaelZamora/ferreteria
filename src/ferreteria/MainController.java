/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import com.jfoenix.controls.JFXButton;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author rafaelantonio
 */
public class MainController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private BorderPane BorderPane;

    @FXML
    private AnchorPane TopPane;

    @FXML
    private Label Titulo;

    @FXML
    private Pane PaneContenido;

    @FXML
    private JFXButton BtnCaja;

    @FXML
    private JFXButton BtnInventario;

    @FXML
    private JFXButton BtnAgregarProducto;

    @FXML
    private JFXButton BtnSalir;

    @FXML
    private JFXButton BtnReporte;

    @FXML
    private JFXButton BtnOpciones;

    @FXML
    private Label lblNombre;

    public static String Nombre;

    public static int IdVendedor;

    public static int Jerarquia;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            CargarPrincipal();
            this.lblNombre.setText(Nombre);
        } catch (IOException ex) {

        }
    }

    @FXML
    void MostrarAgregar(MouseEvent event) throws IOException {
        if (Jerarquia < 3) {
            Pane Pane = FXMLLoader.load(getClass().getResource("AgregarProducto.fxml"));
            this.PaneContenido.getChildren().setAll(Pane);
        } else {
            Alertas.MostrarAlertas(8, this.StackPane);
        }
    }

    @FXML
    void MostrarCaja(MouseEvent event) throws IOException {
        Pane Pane = FXMLLoader.load(getClass().getResource("CajaRegistradora.fxml"));
        this.PaneContenido.getChildren().setAll(Pane);
    }

    @FXML
    void MostrarInventario(MouseEvent event) throws IOException {
        Pane Pane = FXMLLoader.load(getClass().getResource("Inventario.fxml"));
        this.PaneContenido.getChildren().setAll(Pane);
    }

    void CargarPrincipal() throws IOException {
        Pane Pane = FXMLLoader.load(getClass().getResource("CajaRegistradora.fxml"));
        this.PaneContenido.getChildren().setAll(Pane);
    }

    @FXML
    void MostrarReporte(MouseEvent event) throws IOException {
        if (Jerarquia == 1) {
            Pane Pane = FXMLLoader.load(getClass().getResource("Reporte.fxml"));
            this.PaneContenido.getChildren().setAll(Pane);
        } else {
            Alertas.MostrarAlertas(8, this.StackPane);
        }
    }

    @FXML
    void MostrarOpciones(MouseEvent event) throws IOException {
        if (Jerarquia == 1) {
            Pane Pane = FXMLLoader.load(getClass().getResource("Opciones.fxml"));
            this.PaneContenido.getChildren().setAll(Pane);
        } else {
            Alertas.MostrarAlertas(8, this.StackPane);
        }
    }

    @FXML
    void Salir(MouseEvent event) throws IOException {
        Stage stage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
}
