/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class OpcionesController implements Initializable {

    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private JFXButton BtnEliminar;

    @FXML
    private JFXButton BtnAumentar;

    @FXML
    private JFXButton BtnEditar;
    
    @FXML
    private JFXTextField txtMargen;
    
    @FXML
    private Label lbl;

    @FXML
    private Label lblMargen;

    @FXML
    private JFXTreeTableView<?> TreeView;
    
    public static final ObservableList<?> LEmpleados = FXCollections.observableArrayList();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        CajaRegistradoraController.LProductos.clear();
        LEmpleados.clear();
        try {
            Connection con = Conexion.getConnection();
            ResultSet rs = con.createStatement().executeQuery("Select Aviso from Alerta");
            while(rs.next()){
                this.lblMargen.setText(rs.getString("Aviso") + "%");
            }
        } catch (SQLException ex) {
            Logger.getLogger(OpcionesController.class.getName()).log(Level.SEVERE, null, ex);
        }
       /* JFXTreeTableColumn<?, String> idEmpleado = new JFXTreeTableColumn<>("Nº Empleado");
        idEmpleado.setPrefWidth(100);
        idEmpleado.setCellValueFactory((TreeTableColumn.CellDataFeatures<Empleado, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetIdEmpleado());
        JFXTreeTableColumn<?, String> Nombre = new JFXTreeTableColumn<>("Nombre");
        Nombre.setPrefWidth(350);
        Nombre.setCellValueFactory((TreeTableColumn.CellDataFeatures<Empleado, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetNombre());
        JFXTreeTableColumn<?, String> Usuario = new JFXTreeTableColumn<>("Usuario");
        Usuario.setPrefWidth(150);
        Usuario.setCellValueFactory((TreeTableColumn.CellDataFeatures<Empleado, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetUsuario());
        JFXTreeTableColumn<?, String> Jerarquia = new JFXTreeTableColumn<>("Nivel de jerarquía");
        Jerarquia.setPrefWidth(150);
        Jerarquia.setCellValueFactory((TreeTableColumn.CellDataFeatures<Empleado, String> param) -> (ObservableValue<String>) param.getValue().getValue().SgetJerarquia());
        try {
            Connection con = Conexion.getConnection();
            ResultSet rs = con.createStatement().executeQuery("select idVendedor, Nombre, Usuario, NumJerarquia As Jerarquia from Vendedor;");
            while (rs.next()) {
                LEmpleados.add(new Empleado(String.valueOf(rs.getInt("idVendedor")), rs.getString("Nombre"), rs.getString("Usuario"), String.valueOf(rs.getInt("Jerarquia"))));
            }
        } catch (SQLException ex) {
            Logger.getLogger(InventarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
        final TreeItem<Empleado> root = new RecursiveTreeItem<>(LEmpleados, RecursiveTreeObject::getChildren);
        this.TreeView.getColumns().setAll(idEmpleado, Nombre, Usuario, Jerarquia);
        this.TreeView.setRoot(root);
        this.TreeView.setShowRoot(false);*/
    }    
    
    @FXML
    void Modificar(MouseEvent event) throws IOException {
        Stage Crear = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("ModificarPrecio.fxml"));
        Scene scene = new Scene(root);
        Crear.setScene(scene);
        Crear.setResizable(false);
        Crear.initStyle(StageStyle.UNDECORATED);
        Crear.show();
    }

    @FXML
    void Editar(MouseEvent event) throws IOException {
        Stage Crear = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("ModificarUsuario.fxml"));
        Scene scene = new Scene(root);
        Crear.setScene(scene);
        Crear.setResizable(false);
        Crear.initStyle(StageStyle.UNDECORATED);
        Crear.show();
    }

    @FXML
    void Eliminar(MouseEvent event) throws IOException {
        Stage Crear = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("EliminarEmpleado.fxml"));
        Scene scene = new Scene(root);
        Crear.setScene(scene);
        Crear.setResizable(false);
        Crear.initStyle(StageStyle.UNDECORATED);
        Crear.show();
    }
    
    @FXML
    void Margen(KeyEvent event) throws SQLException {
        
    }
    
}
