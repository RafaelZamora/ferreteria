/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author rafaelantonio
 */
public class CajaRegistradoraController implements Initializable {

    @FXML
    private AnchorPane RootPane;

    @FXML
    private JFXTreeTableView<?> TreeView;

    @FXML
    private Label Lbl;

    @FXML
    private Label LblTotal;

    @FXML
    private JFXTextField TXTBuscar;

    @FXML
    private JFXButton BtnCancelar;

    @FXML
    private JFXButton BtnPagar;

    @FXML
    private JFXCheckBox ChxCodigo;
    
    @FXML
    private JFXTextField TxtEliminar;
    

    public static final ObservableList<?> LProductos = FXCollections.observableArrayList();

    public static double Total = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    void Agregar(KeyEvent event) {

    }
    
    @FXML
    void Cancelar(MouseEvent event) {
        this.LProductos.clear();
        this.LblTotal.setText("0");
        this.TXTBuscar.clear();
        this.Total = 0;
    }

    @FXML
    void Finalizar(MouseEvent event) throws IOException {
        if (LProductos.size() > 0) {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Cobro.fxml"));
            Parent rootPago = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(rootPago));
            stage.show();
            
        }
    }
    
    @FXML
    void Eliminar(KeyEvent event) {
        if (event.getCode().equals(KeyCode.ENTER)) {
            if ("".equals(this.TxtEliminar.getText())) {
                
            } else {
                this.LblTotal.setText(String.valueOf(Total));
                LProductos.remove(Integer.parseInt(this.TxtEliminar.getText()) - 1);
                this.TxtEliminar.clear();
            }
        }
    }
}
