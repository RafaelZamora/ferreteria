/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ferreteria;

import Objetos.Conexion;
import com.jfoenix.controls.*;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author RafaelZamora
 */
public class CrearUsuarioController implements Initializable {
    
    @FXML
    private StackPane StackPane;

    @FXML
    private AnchorPane root;

    @FXML
    private Label Lbl;

    @FXML
    private JFXTextField TxtNombre;

    @FXML
    private JFXTextField TxtUsuario;

    @FXML
    private JFXTextField TxtPass;

    @FXML
    private JFXComboBox<Integer> CbxJerar;

    @FXML
    private JFXButton BtnRegistrar;

    @FXML
    private JFXButton BtnCancelar;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            CargarJerarquias();
        } catch (SQLException ex) {
            Logger.getLogger(CrearUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void CargarJerarquias() throws SQLException {
        Connection con = Conexion.getConnection();
        ResultSet rs = con.createStatement().executeQuery("select Distinct NumJerarquia from Jerarquia where NumJerarquia > 1");
        while (rs.next()) {
            this.CbxJerar.getItems().add(rs.getInt("NumJerarquia"));
        }
    }

    @FXML
    void Cancelar(MouseEvent event) {
        ((Node) event.getSource()).getScene().getWindow().hide();
    }

    @FXML
    void Registrar(MouseEvent event) {
        try {
            Connection con = Conexion.getConnection();
            PreparedStatement statement;
            statement = con.prepareStatement("insert into Vendedor (Nombre, Password, Usuario, NumJerarquia) values (?, sha1(?), ?, ?)");
            statement.setString(1, this.TxtNombre.getText());
            statement.setString(2, this.TxtPass.getText());
            statement.setString(3, this.TxtUsuario.getText());
            statement.setInt(4, this.CbxJerar.getValue());
            statement.executeUpdate();
        } catch (Exception ex) {
            Alertas.MostrarAlertas(5, this.StackPane);
            Logger.getLogger(CrearUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        Alertas.MostrarAlertas(4, this.StackPane);
    }

}
